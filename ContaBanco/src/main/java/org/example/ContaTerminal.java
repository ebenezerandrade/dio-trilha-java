package org.example;


import java.math.BigDecimal;
import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numeroConta;
        String agencia;
        String nomeCliente;
        BigDecimal saldo;

        System.out.println("Por favor, digite o número da Conta!");
        numeroConta = scanner.nextInt();
        System.out.println("Por favor, digite o número da Agencia!");
        agencia = scanner.next();
        System.out.println("Por favor, digite o Nome do Cliente!");
        nomeCliente = scanner.next();
        System.out.println("Por favor, digite o Saldo!");
        saldo = scanner.nextBigDecimal();

        System.out.println("Olá "
                + nomeCliente +", obrigado por criar uma conta em nosso banco, sua agência é "
                + agencia + ", conta "
                + numeroConta + " e seu saldo "
                + saldo + " já está disponível para saque");
    }
}
